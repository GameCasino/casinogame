﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class img_carousel : MonoBehaviour {
    public GameObject[] images;
    int currentImage;
    public float slideShowTime;
    float showTime;

    void Start()
    {
        images[0].SetActive(true);
        currentImage = 0;
    }

    // Update is called once per frame
    void Update()
    {

        showTime -= Time.deltaTime;

        if (showTime <= 0)
        {
            showTime = slideShowTime;
            images[currentImage].SetActive(false);
            currentImage++;

            if (currentImage > images.Length - 1)
                currentImage = 0;

            images[currentImage].SetActive(true);
        }

    }
}
